<?php

namespace App\Repository;

use App\Entity\Comment;
use App\Entity\Task;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Task|null find($id, $lockMode = null, $lockVersion = null)
 * @method Task|null findOneBy(array $criteria, array $orderBy = null)
 * @method Task[]    findAll()
 * @method Task[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaskRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Task::class);
    }

    public function getTasksWithMaxCommentData()
    {
        $subQuery = $this->_em->createQueryBuilder()
            ->select('c.id')
            ->from(Comment::class, 'c')
            ->where('c.task = task')
            ->orderBy('length(c.text)', 'DESC')
            ->addOrderBy('c.date', 'DESC');

        return $this->createQueryBuilder('task')
            ->select('task.id, task.name, author.email, length(comments.text) as commentLength ')
            ->leftJoin(
                'task.comments',
                'comments',
                Join::WITH,
                'comments.id = FIRST(' . $subQuery->getDQL() . ') '
            )
            ->leftJoin('comments.author', 'author')
            ->groupBy('task.id, task.name, author.email, commentLength ')
            ->getQuery()
            ->getResult();
    }
}
