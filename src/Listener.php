<?php
/**
 * Created by PhpStorm.
 * User: taras
 * Date: 02.11.18
 * Time: 0:48
 */

namespace App;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class Listener extends \Gedmo\Blameable\BlameableListener
{
    /** @var TokenStorage */
    protected $securityStorage;

    /**
     * @param object $meta
     * @param string $field
     * @param $eventAdapter
     * @return mixed|null|object|string
     */
    public function getFieldValue($meta, $field, $eventAdapter)
    {
        if($this->securityStorage) {
            return $this->securityStorage->getToken()->getUser();
        }

        return null;
    }

    /**
     * @param $securityStorage
     */
    public function setSecurityStorage($securityStorage)
    {
        $this->securityStorage = $securityStorage;
    }
}