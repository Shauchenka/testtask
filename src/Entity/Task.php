<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TaskRepository")
 */
class Task
{
    const STATUS_NEW       = 0;
    const STATUS_ACTIVE    = 1;
    const STATUS_COMPLETED = 2;

    const STATUS_LIST = [
        self::STATUS_NEW       => 'New',
        self::STATUS_ACTIVE    => 'Active',
        self::STATUS_COMPLETED => 'Completed',
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $description;

    /**
     * @var integer
     * @ORM\Column(type="integer", options={"default": 0})
     */
    private $status;

    /**
     * @var User
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $author;

    /**
     * @var User
     * @ORM\ManyToMany(targetEntity="App\Entity\User", inversedBy="tasks")
     * @ORM\JoinTable(name="user_tasks")
     */
    private $executors;

    /**
     * @var
     * @ORM\OneToMany(targetEntity="App\Entity\Comment", mappedBy="task", cascade={"remove"})
     */
    private $comments;

    /**
     * Task constructor.
     * @param $id
     */
    public function __construct()
    {
        $this->status = self::STATUS_NEW;
        $this->comments = new ArrayCollection();
        $this->executors = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function getStatusName(): string
    {
        return self::STATUS_LIST[$this->getStatus()] ?? '';
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): self
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getExecutors(): Collection
    {
        return $this->executors;
    }

    public function clearExecutors()
    {
        $this->executors = new ArrayCollection();

        return $this;
    }

    public function addExecutor(User $executor): self
    {
        if (!is_null($executor) && !$this->executors->contains($executor)) {
            $this->executors[] = $executor;
        }

        return $this;
    }

    public function removeExecutor(User $executor): self
    {
        if ($this->executors->contains($executor)) {
            $this->executors->removeElement($executor);
        }

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setTask($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getTask() === $this) {
                $comment->setTask(null);
            }
        }

        return $this;
    }
}
