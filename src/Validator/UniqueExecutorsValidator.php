<?php
/**
 * Created by PhpStorm.
 * User: taras
 * Date: 05.11.18
 * Time: 1:56
 */

namespace App\Validator;


use App\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class UniqueExecutorsValidator extends ConstraintValidator
{

    public function validate($users, Constraint $constraint)
    {
        $isValid = true;

        if(!$users instanceof Collection) {
            $this->context->buildViolation('Incorrect data')->addViolation();
        }

        $uniqueIds = [];

        /** @var User $user */
        foreach ($users as $user) {
            if(is_null($user)) {
                continue;
            }

            if(!in_array($user->getId(), $uniqueIds)) {
               array_push($uniqueIds, $user->getId());
            } else {
                $this->context->buildViolation($constraint->message)->setParameter('{{ email }}', $user->getEmail())->addViolation();
                $isValid =  false;
            }
        }

        return $isValid;
    }

}