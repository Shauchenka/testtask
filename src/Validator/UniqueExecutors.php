<?php
/**
 * Created by PhpStorm.
 * User: taras
 * Date: 05.11.18
 * Time: 1:59
 */

namespace App\Validator;


use Symfony\Component\Validator\Constraint;

class UniqueExecutors extends Constraint
{
    public $message = 'User with email {{ email }} is duplicated';
}