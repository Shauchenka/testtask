<?php
/**
 * Created by PhpStorm.
 * User: taras
 * Date: 02.11.18
 * Time: 1:38
 */

namespace App\Form;


use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'class' => User::class,
            'label' => false,
        ]);
    }

    public function getParent()
    {
        return EntityType::class;
    }
}