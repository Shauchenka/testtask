<?php

namespace App\Form;

use App\Entity\Task;
use App\Entity\User;
use App\Validator\UniqueExecutors;
use Doctrine\DBAL\Types\ArrayType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TaskType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('description', TextareaType::class)
            ->add('status', ChoiceType::class, [
                'choices' => array_flip(Task::STATUS_LIST)
            ])
            ->add('executors', CollectionType::class, [
                'entry_type'   => UserType::class,
                'allow_add'    => true,
                'allow_delete' => true,
                'label'        => false,
                'constraints'  => [
                    new UniqueExecutors()
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Task::class,
        ]);
    }
}
