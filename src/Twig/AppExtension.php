<?php
/**
 * Created by PhpStorm.
 * User: taras
 * Date: 05.11.18
 * Time: 5:11
 */

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class AppExtension extends AbstractExtension
{
    public function getFilters()
    {
        return array(
            new TwigFilter('noData', array($this, 'getNoData')),
        );
    }

    public function getNoData(?string $value)
    {
        return $value ?? "NO DATA";
    }


}